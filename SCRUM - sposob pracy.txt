A. NA ZAJ�CIACH

Organizacji pracy w grupie - ustalenie niezb�dnych do wykonania
1Przydzielenie zada� dla poszczeg�lnych os�b, ustalenie kto w danym tygodniu b�dzie pe�ni� rol� Scrum Mastera.
Zaplanowaniu najbli�szego Sprintu (czyli wydania newslettera) - stworzenie ticket�w, subticket�w
Oszacowanie czasu poszczeg�lnych ticket�w i subticket�w (uwaga: zamiast szacunku godzinnego, kt�ry jest domy�lny dla Assembli, przyjmujemy szacunek minutowy. Czyli np. "Worked hours 10" oznacza przepracowanych 10 minut).
Ustalenie odpowiedzialno�ci za poszczeg�lne zadania.
W projekcie maj� by� cztery Milestone'y, ka�dy z terminem przed rozpocz�ciem zaj��.
Stany ticket�w: New - oznacza wszystkie tickety. Accepted - oznacza, �e u�ytkownik przyj�� do realizacji zadanie i aktualnie je realizuje. Test - oznacza, �e poprawiamy (testujemy) dan� funkcjonalno��.
Ticket "Scrum Master" - w zespole powinna by� osoba odpowiedzialna za rozwi�zanie powsta�ych problem�w w zespole (je�li si� pojawi�), np. blokada repozytorium, brak dost�pu do internetu jednej z os�b w zespole itp.
Ustalenie koncepcji projektu. Jak podzieli� si� zadaniami? Czy wcze�niej potrzebny jest tzw. research? Kto ma tym si� zaj��?
Zak�adamy prac� od poniedzia�ku do pi�tku (sobota, niedziela - dni wolne, bez raportowania).


B. W CI�GU NAJBLI�SZEGO TYGODNIA

Codziennie (pn-pt): daily Scrum (w formie Standup'a) - ka�dy cz�onek zespo�u ma codziennie do ko�ca Sprintu umieszcza� na Assembli informacje o tym co robi�, co b�dzie robi� oraz czy ma problemy (raportowanie)
Korzystanie z repozytorium GIT. Ka�da osoba zobowi�zana jest korzysta� ze wsp�lnego repozytorium oraz dodawa� tre�ci uwzgl�dniaj�c swoje konto.
Obowi�zkowe commity - ka�da osoba w zespole ma wykona� co najmniej 5 commit�w w ramach ca�ego projektu (jest to absolutne minimum). Dobrze wykonane zadanie: commit raz na dzie� (czyli: 20 commit�w). Ka�dy commit powinien dotyczy� konkretnego zadania/podzadania.
Je�li pojawi� si� konflikty w repozytorium, odpowiedzialno�� za ich rozwi�zanie spoczywa na osobie, u kt�rej powsta� konflikt, ew. prosi o pomoc Scrum Master'a. Brak rozwi�zania konfliktu przez 1 osob� przez kilka dni oznacza z�� organizacj� pracy w zespole i rzutuje na ocen� pracy grupy!
Ka�da wiadomo�� powinna by� opatrzona kr�tkim opisem oraz linkami do zewn�trznych �r�de� (np. jednego lub kilku portali internetowych).



C. Co podlega ocenie?

organizacja pracy w zespole, przydzia� zada� do poszczeg�lnych os�b, r�wnomierne obci��enie cz�onk�w zespo�u
praca indywidualna: codzienne standupy (lub informacja o tym, �e kogo� nie b�dzie na nast�pnym itp.), commity powi�zane z ticketami, umiej�tno�� zmiany stanu ticketu (np. accepted - aktualnie realizowane), aktualizacje czasu sp�dzonego na danym zadaniu
radzenie sobie z wynik�ymi problemami (np. nieobecno��/brak dost�pu do internetu osoby w zespole, umiej�tno�� rozwi�zania konfliktu w SVN przez osob� lub z pomoc� Scrum Mastera lub innych cz�onk�w zespo�u).
komunikacja w zespole z wykorzystaniem Assembli
uzyskany produkt finalny, czyli efekt ko�cowy - prosz� zwr�ci� uwag�, �e jest to tylko jeden z wielu czynnik�w
#include "gtest/gtest.h"
#include "kod.hpp"

TEST(GRA, plansza) {
    ASSERT_EQ(plansza(null),
    "   |   |   \n"
    "---+---+---\n"
    "   |   |   \n"
    "---+---+---\n"
    "   |   |   \n"
    );
}


TEST(GRA, remis) {

    for(int i = 1; i <= 9; i++)
        t[i] = 'O';
    ASSERT_EQ(remis (t[]), true);

}


TEST(GRA, wygrana) {

    for(int i = 1; i <= 3; i++)
        t[i] = 'O';
    ASSERT_EQ(wygrana (t[], 'O'), true);

}

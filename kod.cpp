#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <ctime>
using namespace std;

void plansza(char t[])
{
    for(int i = 1; i <= 9; i++)
    {
        cout << " " << t[i] << " ";
        if(i % 3)
            cout << "|";
        else if(i != 9)
            cout << "\n---+---+---\n";
        else cout << endl;
    }
}

bool wygrana(char t[], char g)
{
    bool test;
    int i;
    test = false;
    
    for(i = 1; i <= 7; i += 3) 
        test |= ((t[i] == g) && (t[i+1] == g) && (t[i+2] == g));
    for(i = 1; i <= 3; i++)
        test |= ((t[i] == g) && (t[i+3] == g) && (t[i+6] == g));
    test |= ((t[1] == g) && (t[5] == g) && (t[9] == g));
    test |= ((t[3] == g) && (t[5] == g) && (t[7] == g));
    if(test)
    {
        plansza(t);
        cout << "\nGRACZ " << g << " WYGRYWA!!!\n\n";
        return true;
    }
    return false;
}

bool remis(char t[])
{
  for(int i = 1; i <= 9; i++)
    if(t[i] == ' ') return false;
  plansza(t);
  cout << "\nREMIS !!!\n\n";
  return true;     
}

void ruch(char t[], char &gracz, string imie)
{
    int r;

    plansza(t);
    cout << "\nGRACZ " << gracz << ", "<< imie << " : Twoj ruch : ";
    cin >> r;
    cout << "-----------------------\n\n";
    if((r >= 1) && (r <= 9) && (t[r] == ' ')) t[r] = gracz;
    gracz = (gracz == 'O') ? 'X' : 'O';

}

void ruch(char t[], char &gracz)
{
    int r;

    plansza(t);
    cout << "\nGRACZ " << gracz << " : Twoj ruch : ";
    cin >> r;
    cout << "-----------------------\n\n";
    if((r >= 1) && (r <= 9) && (t[r] == ' ')) t[r] = gracz;
    gracz = (gracz == 'O') ? 'X' : 'O';
}

int minimax(char t[], char gracz)
{
    if(wygrana(t,gracz)) return (gracz == 'X') ? 1 : -1;
    if(remis(t)) return 0;

    int m, mmx;
    gracz = (gracz == 'X') ? 'O' : 'X';
    mmx = (gracz == 'O') ? 10 : -10;

    for(int i = 1; i <= 9; i++)
        if(t[i] == ' ')
        {
            t[i] = gracz;
            m = minimax(t,gracz);
            t[i] = ' ';
            if(((gracz == 'O') && (m < mmx)) || ((gracz == 'X') && (m > mmx))) mmx = m;
        }
    return mmx;
}


int ruch_komputera(char t[], char k)
{
    int ruch, i, m, mmx;

    mmx = -10;
    for(i = 1; i <= 9; i++)
        if(t[i] == ' ')
        {
            t[i] = k;
            m = minimax(t,k);
            t[i] = ' ';
            if(m > mmx)
            {
                mmx = m; ruch = i;
            }
        }

    plansza(t);
    cout << "\nKomputer wykonuje ruch : ";
    cout << "-----------------------\n\n";
    t[ruch] = k;
}



/*void SetColor(string number) {

    int color = 0;

    switch (number[0]) {

        case '0':

            color += 0x40;
            break;

        case '1':

            color += 0x20;
            break;

        case '2':

            color += 0x10;
            break;

        case '3':

            color += 0x60;
            break;

        case '4':

            color += 0x50;
            break;

        case '5':

            color += 0x30;
            break;

        case '6':

            color += 0x70;
            break;

        case '7':

            color += 0xC0;
            break;

        case '8':

            color += 0xA0;
            break;

        case '9':

            color += 0x90;
            break;

        case 'A':

            color += 0xE0;
            break;

        case 'B':

            color += 0xD0;
            break;

        case 'C':

            color += 0xB0;
            break;

        case 'D':

            color += 0xF0;
            break;

    }


    switch (number[1]) {

        case '0':

            color += 0x4;
            break;

        case '1':

            color += 0x2;
            break;

        case '2':

            color += 0x1;
            break;

        case '3':

            color += 0x6;
            break;

        case '4':

            color += 0x5;
            break;

        case '5':

            color += 0x3;
            break;

        case '6':

            color += 0x7;
            break;

        case '7':

            color += 0xC;
            break;

        case '8':

            color += 0xA;
            break;

        case '9':

            color += 0x9;
            break;

        case 'A':

            color += 0xE;
            break;

        case 'B':

            color += 0xD;
            break;

        case 'C':

            color += 0xB;
            break;

        case 'D':

            color += 0xF;
            break;

    }

    //SetConsoleTextAttribute( !!!!, color);

}
*/ /*
void kalejdoskop (){
    srand(time(0));
    string tab[]={"0", "1", "2", "3", "4", "5", "6", "7", "8","9","A", "B", "C", "D"};
    int wylosowana=rand() % 14;
    srand(time(0));
    int los=rand() % 14;
    string str = tab[wylosowana]+tab[los];
    SetColor(str);
}
*/
int main ()
{
    string str;
    string odp;
    int i;
    int punkt_O = 0;
    int punkt_X = 0;
  /*  cout << "Czy chcesz zmienic kolor tla i tekstu? TAK/NIE\n";
    cin >> odp;
    if (odp=="TAK") {
        cout << "W celu uzyskania instrukcji wcisnij 1\n";
        cin >>i;
        if(i==1){
            cout << "Wybierz kolor wedlug ponizszego kodu:\n Pierwszy znak odpowiada za kolor tla pod tekstem, drugi za kolor tekstu\n Kolory podstawowe\n0 - czerwony\n \n 1 - zielony\n 2 - niebieski\n 3 - pomaranczowy\n 4 - fioletowy\n 5 - zolty\n 6 - bialy\n \n Kolory jaskrawe\n 7 - czerwony jaskrawy\n 8 - zielony jaskrawy\n 9 - niebieski jaskrawy\n A - pomaranczowy\n B - fioletowy jaskrawy\n C - zolty jaskrawy\n D - bialy jaskrawy\n\n";
        }
        cout << "Wpisz kolor tla i kolor tekstu (od 0-9 + A-D) w formacie np 2A:\n";
        cin >> str;

        SetColor(str);
    }
    cout << "Czy chcesz uruchomic tryb wielokolorowy? TAK/NIE\n";
    cin >> odp;
    int kol = -1;
     if (odp=="TAK") {
        kol = 0;
    } */
    char p[10],g, w;
    string imie1, imie2, imie;
    int k;
    cout<< "Jesli chcesz grac z innym graczem wybierz 4, jesli chcesz grac z komputerem wybierz 5\n"<< endl;
    cin >> k;
    if (k==4){
        
            cout << "Kolko i Krzyzyk\n"
                    "dla dwoch graczy\n"
                    " \n"

                    "aby wybrac pozycje na planszy uzyc znakow od 1 do 9\n\n"
                    "Wpisz imie 1 gracza:\n";
            cin >> imie1;
            cout << "Wpisz imie gracza 2:\n";
            cin >> imie2;
        do {
            for (int i = 1; i <= 9; i++) p[i] = ' ';
            g = 'O';
            imie = imie1;
            while (!wygrana(p, 'X') && !wygrana(p, 'O') && !remis(p)) {
                imie = (imie == imie1) ? imie2 : imie1,
                ruch(p, g, imie);
                /*if (kol == 0){
                    kalejdoskop ();
                }*/
                }
                            
            if (wygrana(p, 'X') == true)
                {
                    punkt_X++;
                }
                
            else if (wygrana(p, 'O') == true)
                {
                    punkt_O++;
                }
                
            cout << imie1 << " ma " << punkt_X << " punktow" << endl;
            cout << imie2 << " ma " << punkt_O << " punktow" << endl;


            cout << "Jeszcze raz ? (T = TAK) : ";
            cin >> w;
            cout << "\n\n\n";
            /*if (kol == 0){
                    kalejdoskop ();
                }*/
        } while((w == 'T') || (w == 't'));
    }
    if(k==5){
        do
        {
            cout <<  "Kolko i Krzyzyk\n"
                     "gracz versus komputer\n"
                     " _ _ _ _\n"

                     "Aby wybrac pozycje na planszy uzyc znakow od 1 do 9\n\n";
            for(int i = 1; i <= 9; i++) p[i] = ' ';
            g = 'O';
            char komp = 'X'; // pomocniczy znacznik komputera

            while(TRUE){
                if(wygrana(p,'X') || remis(p)) break;
                ruch(p,g);
                if(wygrana(p,'O') || remis(p)) break;
                ruch_komputera(p,komp);
                /*if (kol == 0){
                    kalejdoskop ();
                }*/
            }
            cout << "Jeszcze raz? (T = TAK) : ";
            cin >> w;
            cout << "\n\n\n";
            /*if (kol == 0){
                    kalejdoskop ();
                }*/
        } while((w == 'T') || (w == 't'));
    }

}